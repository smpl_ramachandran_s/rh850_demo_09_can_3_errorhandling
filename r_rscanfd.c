/*===========================================================================*/
/* Project:  F1x StarterKit V3 Sample Software                               */
/* Module :  r_rscanfd.c                                                     */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2016 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
/*                                                                           */
/* Source code for RSCAN-FD functions.                                       */
/*                                                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Includes */
/*===========================================================================*/
#include "r_cg_macrodriver.h"
#include "r_rscanfd.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Globals
 ******************************************************************************/
uint32_t gu32CanBuadRateReg = 0x0A1B5000U;      /* 1000 KBPS */         // 000 01010 0 0011011 01010 0 0000000000
                                                                        // SEG2: 10, SEG1: 27, SJW: 10 (all Tq), NBRP: 0
/*===========================================================================*/
/* Rx Rule Table */
/*===========================================================================*/

/*
Receive Rule ID Register: For the standard ID, set the ID in bits b10 to b0, ext ID: b28 to b0

Receive Rule Mask Register: set to 1--The corresponding bit is compared

Receive Rule Pointer 0 Register: 31to28 Receive Rule DLC check, receive buffer assignment

Receive Rule Pointer 1 Register: FIFO buffer assignment
*/

canfd_cre_type CANFD_RX_RULE_TABLE[8] = {
    /* Set receive rules i.e. configure registers RCFDCnCFDGAFLIDj, RCFDCnCFDGAFLMj, RCFDCnCFDGAFLP0_j, RCFDCnCFDGAFLP1_j */
#if (USE_RX_FIFO_BUFFER == 1)
    /* Configuration for receive FIFO buffers */
    {0x00000000UL, 0xDFFFF800UL, 0xB0000000UL, 0x00000001UL}, /* NO.000 : std data frm ID = 0x000 to 0x7FF will be accepted, destination Rx Buffer0 */
    {0x00000010UL, 0xDFFFFFF0UL, 0xB0000000UL, 0x00000001UL}, /* NO.001 : std data frm ID = 0x10 to 0x1F will be accepted, destination Rx Buffer0 */
    {0x00000020UL, 0xDFFFFFF0UL, 0xB0000000UL, 0x00000001UL}, /* NO.002 : std data frm ID = 0x20 to 0x2F will be accepted, destination Rx Buffer1 */
    {0x00000030UL, 0xDFFFFFF0UL, 0xB0000000UL, 0x00000001UL}, /* NO.003 : std data frm ID = 0x30 to 0x3F will be accepted, destination Rx Buffer2 */
    {0x00000040UL, 0xDFFFFFF0UL, 0xB0000000UL, 0x00000001UL}, /* NO.004 : std data frm ID = 0x40 to 0x4F will be accepted, destination Rx Buffer3 */
    {0x00000050UL, 0xDFFFFFF0UL, 0xB0000000UL, 0x00000001UL}, /* NO.005 : std data frm ID = 0x50 to 0x5F will be accepted, destination Rx Buffer4 */
    {0x00000060UL, 0xDFFFFFFFUL, 0xB0000000UL, 0x00000001UL}, /* NO.006 : std data frm ID = 0x60 will be accepted, destination Rx Buffer5 */
    {0x00000070UL, 0xDFFFFFFFUL, 0xB0000000UL, 0x00000001UL}, /* NO.007 : std data frm ID = 0x70 will be accepted, destination Rx Buffer6 */
#else
    /* Configuration for receive buffers */
    {0x00000010UL, 0xDFFFFFF0UL, 0xB0008000UL, 0x00000000UL}, /* NO.000 : std data frm ID = 0x10 to 0x1F will be accepted, destination Rx Buffer0 */
    {0x00000020UL, 0xDFFFFFF0UL, 0xB0008100UL, 0x00000000UL}, /* NO.001 : std data frm ID = 0x20 to 0x2F will be accepted, destination Rx Buffer1 */
    {0x00000030UL, 0xDFFFFFF0UL, 0xB0008200UL, 0x00000000UL}, /* NO.002 : std data frm ID = 0x30 to 0x3F will be accepted, destination Rx Buffer2 */
    {0x00000040UL, 0xDFFFFFF0UL, 0xB0008300UL, 0x00000000UL}, /* NO.003 : std data frm ID = 0x40 to 0x4F will be accepted, destination Rx Buffer3 */
    {0x00000050UL, 0xDFFFFFFFUL, 0xB0008400UL, 0x00000000UL}, /* NO.004 : std data frm ID = 0x50 will be accepted, destination Rx Buffer4 */
    {0x00000060UL, 0xDFFFFFFFUL, 0xB0008500UL, 0x00000000UL}, /* NO.005 : std data frm ID = 0x60 will be accepted, destination Rx Buffer5 */
    {0x00000070UL, 0xDFFFFFFFUL, 0xB0008600UL, 0x00000000UL}, /* NO.006 : std data frm ID = 0x70 will be accepted, destination Rx Buffer6 */
    {0x00000080UL, 0xDFFFFFFFUL, 0xB0008700UL, 0x00000000UL}, /* NO.007 : std data frm ID = 0x80 will be accepted, destination Rx Buffer7 */

#endif
};

void R_RSCANFD_SetRxRule(void);
/*===========================================================================*/
/* Functions */
/*===========================================================================*/
/*****************************************************************************
** Function:    RS_CANFD_init
** Description: Configures the CAN1 macro
** Parameter:   None
** Return:      None
******************************************************************************/
void R_RSCAN0FD_Init(void)
{

    INTC1.ICRCAN0TRX.BIT.MKRCAN0TRX = _INT_PROCESSING_DISABLED;
    INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = _INT_REQUEST_NOT_OCCUR;

    INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = _INT_PROCESSING_DISABLED;
    INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = _INT_REQUEST_NOT_OCCUR;

    INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = _INT_PROCESSING_DISABLED;
    INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = _INT_REQUEST_NOT_OCCUR;

    /* Wait while CAN RAM initialization is ongoing */
    while((RCFDC0.CFDGSTS.UINT32 & 0x00000008)) ;
    
   /* switch mode to CANFD mode,reset the main registers */
    RCFDC0.CFDGCTR.UINT32 = 0x00000005U;        // global stop mode
   
    /* Switch to global/channel reset mode */
    RCFDC0.CFDGCTR.UINT32 &= 0xfffffffb;	//set the 3rd bit to 0 -- clear GSLPR to 0  from STOP MODE to RESET mode
    RCFDC0.CFDGCTR.UINT32 |= 0x00000001u;  //GMDC[1:0]BIT = 01B  ---ensure from global operation mode to reset mode

    /* wait while global reset mode comes on*/
    while(RCFDC0.CFDGSTS.UINT32 !=0x01);
    
    /* Channel reset mode selected */
    RCFDC0.CFDC0CTR.UINT32 &= 0xfffffffb;

    /* wait while channel reset mode comes on*/
    while (!(RCFDC0.CFDC0STS.UINT32 & 0x01))
    

    /*ID priority,DLC check is disabled,clkc as CAN clock--PPLLCLK2, overflow stored, */
    RCFDC0.CFDGCFG.UINT32 = 0x00001020;   //0B 0001 0000 0010 0000
   
        
    /*  fCAN = clkc = PPLLCLK2 = 40MHz
     *  Bitrate = (fCAN/(BRP+1))/(1+TSEG1+TSEG2) = (40M/1)/40 = 1Mbps 
     */
    RCFDC0.CFDC0NCFG.UINT32 = gu32CanBuadRateReg;
    
    RCFDC0.CFDGAFLCFG0.UINT32 = 0x00000000;     // zero receive rules
    R_RSCANFD_SetRxRule();

#if (USE_RX_FIFO_BUFFER == 1)
    /*Receive FIFO buffer setting*/
    RCFDC0.CFDRFCC0.UINT32 = 0x00000300; /* Buffer depth 16, Data length 8 */
    RCFDC0.CFDRFCC0.UINT32 |= 0x1002;
#else
    /* Receive buffer setting  */ 
    RCFDC0.CFDRMNB.UINT32 = 0x0008; // Receive buffer payload storage size 8 bytes, Number of receive buffers 8
#endif  /* (USE_RX_FIFO_BUFFER == 1) */
	
#if (USE_TX_FIFO_BUFFER == 1)
    RCFDC0.CFDTMC0 = 0x0U;
    /* *Transmit FIFO buffer setting*
       Buffer depth configuration: 16 messages
       Transmit buffer link configuration: Buffer #0 to be linked to transmit/receive FIFO buffer
       Transmit mode is selected
       Payload storage size: 8 bytes
       Transmit/receive FIFO buffers are not used
     */
    RCFDC0.CFDCFCC0.UINT32 = 0x00600100;
#endif  /* (USE_TX_FIFO_BUFFER == 1) */
    /* Set CMPOFIE,THLEIE,MEIE,DEIE disabled*/
    RCFDC0.CFDGCTR.UINT32 &= 0xfffff0ff;
    //RCFDC0.CFDGCTR.UINT32 |= 0x00000f00;
    
    /* Enable All Channel Error Interrupt */
    RCFDC0.CFDC0CTR.UINT32 |= 0x00000100;
    //RCFDC0.CFDC0CTR.UINT32 &= 0xffffbfff;

    
    // set TDC in Global operation mode, in Channel reset mode
    RCFDC0.CFDC0FDCFG.UINT32 |= 0x40000000; /* Enable classical CAN mode only */
    
    /* If GlobalChannel in halt or reset mode */
    if (RCFDC0.CFDGSTS.UINT32 & 0x03) 
    {
        RCFDC0.CFDGCTR.UINT32 &= 0xfffffffc; //Switch to operation mode
        while ((RCFDC0.CFDGSTS.UINT32 & 0x02) == 2) {
            /* While halt mode */
        }
        while ((RCFDC0.CFDGSTS.UINT32 & 0x01) == 1) {
            /* While reset mode */
        }
    }
    
	 
    /* If Channel 0 in halt or reset mode */
    if (RCFDC0.CFDC0STS.UINT32 & 0x03) 
    {
        RCFDC0.CFDC0CTR.UINT32 &= 0xfffffffc;    //Switch to communication mode
        while ((RCFDC0.CFDC0STS.UINT32 & 0x02) == 2) {
            /* While halt mode */
        }
        while ((RCFDC0.CFDC0STS.UINT32 & 0x01) == 1) {
            /* While reset mode */
        }
    }

#if (USE_RX_FIFO_BUFFER == 1)
    /* Following configuration is done in channel communication mode (operation mode). */
    RCFDC0.CFDRFCC0.UINT32 |= 0x01; /* Receive FIFO buffers are used */
#endif  /* (USE_RX_FIFO_BUFFER == 1) */
#if (USE_TX_FIFO_BUFFER == 1)
    RCFDC0.CFDCFCC0.UINT32 |= 0x01; /* Transmit/receive FIFO buffers are used */
#endif  /* (USE_TX_FIFO_BUFFER == 1) */


    INTC1.ICRCAN0TRX.BIT.TBRCAN0TRX = _INT_TABLE_VECTOR;
    INTC1.ICRCAN0TRX.UINT16 &= _INT_PRIORITY_LEVEL2;

    INTC1.ICRCAN0ERR.BIT.TBRCAN0ERR = _INT_TABLE_VECTOR;
    INTC1.ICRCAN0ERR.UINT16 &= _INT_PRIORITY_LEVEL3;

    INTC1.ICRCANGERR0.BIT.TBRCANGERR0 = _INT_TABLE_VECTOR;
    INTC1.ICRCANGERR0.UINT16 &= _INT_PRIORITY_LEVEL3;
    

    INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = _INT_REQUEST_NOT_OCCUR;
    INTC1.ICRCAN0TRX.BIT.MKRCAN0TRX = _INT_PROCESSING_ENABLED;

    INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = _INT_REQUEST_NOT_OCCUR;
    INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = _INT_PROCESSING_ENABLED;

    INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = _INT_REQUEST_NOT_OCCUR;
    INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = _INT_PROCESSING_ENABLED;
}

/******************************************************************************
** Function:    R_RSCANFD_SetRxRule
** Description: Set all Rx rules for CANfd MODE
** Parameter:   None
** Return:      None
******************************************************************************/
void R_RSCANFD_SetRxRule(void)
{    
    uint16_t RxRuleIdx ;
    unsigned char PageRxRuleIdx;
    volatile CANFD_RULE_Type* pCRE;

    /* Set Rx rule number per channel */
    RCFDC0.CFDGAFLCFG0.UINT32 |= 0x08000000;   // 8 rules can be set for channel 0
    
    /* Get access base address of Rx rule */
    pCRE = (volatile CANFD_RULE_Type*)&(RCFDC0.CFDGAFLID0.UINT32);

    /* Receive Rule Table Write Enable */
    RCFDC0.CFDGAFLECTR.UINT32 |= 0x00000100;	//set bit8 to 1, Receive rule table write is enabled

    /* Copy Rx rule one by one */
    for (RxRuleIdx = 0U; RxRuleIdx < 8U; RxRuleIdx++)
    {
        /* If it is more than 16 rules, go to another page */
        PageRxRuleIdx = (uint8_t) (RxRuleIdx & 0x0F); //CAN_PAGE_RX_RULE_IDX_MASK= 0xF

        /* Update target Rx rule page if necessary. */
        if (PageRxRuleIdx == 0U) //RxRuleIdx=0, page=0; RxRuleIdx=16, page=1; RxRuleIdx=32,page=2;...
        {
            RCFDC0.CFDGAFLECTR.UINT32 |= RxRuleIdx >> 4U; //CAN_RX_RULE_PAGE_IDX_BIT_POS= 4U
        }

        /* Set a single Rx rule.*/
        pCRE[PageRxRuleIdx] = CANFD_RX_RULE_TABLE[RxRuleIdx];
    }

    /* Rx rule write disable */
    RCFDC0.CFDGAFLECTR.UINT32 &= 0xfffffeff;
}

/*****************************************************************************
** Function:    Canfd_ReadRxFIFOBuffer
** Description: This code shows how to read message from Rx FIFO buffer
** Parameter:   CANFD_FrameType* pFrame - the data will be stored to
** Return:      void  
******************************************************************************/
void CANFD_ReadHwRxFIFOBuffer(CANFD_FrameType* pFrame)
{
    // bool bStatus = false;
    CANFD_FrameType* pRxFIFOBuffer ;

    /* Check if data is received in RX HW FIFO buffer 0 */
    if( (RCFDC0.CFDRFSTS0.UINT32 & 0x1U) == 0x0U )
    {
        pRxFIFOBuffer = (CANFD_FrameType*) &(RCFDC0.CFDRFID0.UINT32);
        *pFrame = pRxFIFOBuffer[0];
        RCFDC0.CFDRFPCTR0.UINT32 |= 0xFFU;
        // bStatus = true;
    }

    // return bStatus;
}

CAN_STATUS_t CANFD_TransmitHwTxFIFOBuffer(CANFD_FrameType* pFrame)
{
    uint8_t i;
    CAN_STATUS_t eCanStatus_t;
    /* Check if HW TX FIFO buffer is full */
    if( (RCFDC0.CFDCFSTS0.UINT32 & 0x2U) == 0U )
    {
    /* HW TX FIFO buffer is not full */
  
    /* Set CAN id */
    RCFDC0.CFDCFID0.UINT32 = (RCFDC0.CFDCFID0.UINT32 & 0xE0000000U) | (uint32_t)(pFrame->ID);
    /* Set data length */
    RCFDC0.CFDCFPTR0.UINT32 = (RCFDC0.CFDCFPTR0.UINT32 & 0x0FFFFFFFU) | ((uint32_t)(pFrame->DLC) << 28U);
    /* Set the label as 0x0000
       Configure as classical CAN frame 
       The bit rate in the data area does not change
       Error active node */
    RCFDC0.CFDCFFDCSTS0.UINT32 = 0x0U;

    for(i = 0U; i < pFrame->DLC; i++)
    {
        /* Fill data into register */
        if(i < 4U)
        {
          RCFDC0.CFDCFDF0_0.UINT8[i] = pFrame->DB[i];
        }
        else
        {
          RCFDC0.CFDCFDF1_0.UINT8[i-4U] = pFrame->DB[i];    
        }
    }

    /* Move write pointer to the next stage of the transmit/receive FIFO buffer */
    RCFDC0.CFDCFPCTR0.UINT32 |= 0xFFU;

    eCanStatus_t = CAN_OK;
  }
  else
  {
      /* HW TX FIFO buffer is full */
      eCanStatus_t = CAN_ERR_TX_FIFO_FULL;
  }

  return eCanStatus_t;
}
/*****************************************************************************
** Function:    Can_ReadRx_buffer
** Description: This code shows how to read message from Rx buffer
** Parameter:   pRxBufIdx - Pointer to Rx buffer that receives frame
** Return:      void   
******************************************************************************/
