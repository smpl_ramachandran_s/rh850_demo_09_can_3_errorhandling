/*===========================================================================*/
/* Project:  F1x StarterKit V3 Sample Software                               */
/* Module :  r_rscanfd.h                                                     */
/* Version:  V1.00                                                           */
/*===========================================================================*/
/*                                  COPYRIGHT                                */
/*===========================================================================*/
/* Copyright (c) 2016 by Renesas Electronics Europe GmbH,                    */
/*               a company of the Renesas Electronics Corporation            */
/*===========================================================================*/
/* In case of any question please do not hesitate to contact:                */
/*                                                                           */
/*        ABG Software Tool Support                                          */
/*                                                                           */
/*        Renesas Electronics Europe GmbH                                    */
/*        Arcadiastrasse 10                                                  */
/*        D-40472 Duesseldorf, Germany                                       */
/*                                                                           */
/*        e-mail: software_support-eu@lm.renesas.com                         */
/*        FAX:   +49 - (0)211 / 65 03 - 11 31                                */
/*                                                                           */
/*===========================================================================*/
/* Warranty Disclaimer                                                       */
/*                                                                           */
/* Because the Product(s) is licensed free of charge, there is no warranty   */
/* of any kind whatsoever and expressly disclaimed and excluded by Renesas,  */
/* either expressed or implied, including but not limited to those for       */
/* non-infringement of intellectual property, merchantability and/or         */
/* fitness for the particular purpose.                                       */
/* Renesas shall not have any obligation to maintain, service or provide bug */
/* fixes for the supplied Product(s) and/or the Application.                 */
/*                                                                           */
/* Each User is solely responsible for determining the appropriateness of    */
/* using the Product(s) and assumes all risks associated with its exercise   */
/* of rights under this Agreement, including, but not limited to the risks   */
/* and costs of program errors, compliance with applicable laws, damage to   */
/* or loss of data, programs or equipment, and unavailability or             */
/* interruption of operations.                                               */
/*                                                                           */
/* Limitation of Liability                                                   */
/*                                                                           */
/* In no event shall Renesas be liable to the User for any incidental,       */
/* consequential, indirect, or punitive damage (including but not limited    */
/* to lost profits) regardless of whether such liability is based on breach  */
/* of contract, tort, strict liability, breach of warranties, failure of     */
/* essential purpose or otherwise and even if advised of the possibility of  */
/* such damages. Renesas shall not be liable for any services or products    */
/* provided by third party vendors, developers or consultants identified or  */
/* referred to the User by Renesas in connection with the Product(s) and/or  */
/* the Application.                                                          */
/*                                                                           */
/*===========================================================================*/
/* History:                                                                  */
/*              V1.00: Initial version                                       */
/*                                                                           */
/*===========================================================================*/
#ifndef R_RSCANFD_H
#define R_RSCANFD_H

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "r_cg_macrodriver.h"

  
/*******************************************************************************
 * Typedefs
 ******************************************************************************/

typedef struct
{
    uint32_t ID :29;
    uint32_t THLEN :1;
    uint32_t RTR :1;
    uint32_t IDE :1;
    
    uint32_t TS :16;
    uint32_t LBL :8;
    uint32_t res0 :4;
    uint32_t DLC :4;

    uint32_t ESI :1;
    uint32_t BRS :1;
    uint32_t FDF :1;
    uint32_t res1 :29;
  
    uint8_t DB[ 8 ];
} CANFD_FrameType;


typedef enum 
{
    CAN_OK,
    CAN_ERR_RX_FIFO_FULL,
    CAN_ERR_RX_BUFFER_EMPTY,
    CAN_ERR_TX_TIMEOUT,
    CAN_ERR_TX_IN_PROGRESS,
    CAN_ERR_TX_FIFO_FULL
}CAN_STATUS_t;


typedef struct {
  unsigned long lword[4];
}canfd_cre_type;



/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define CANFD_RULE_Type canfd_cre_type


/* ---- bit value ---- */
#define CAN_SET                             1U
#define CAN_CLR                             0U

/* ---- bit mask ---- */
#define CAN_1_BIT_MASK                      1U
#define CAN_2_BIT_MASK                      3U
#define CAN_3_BIT_MASK                      7U
#define CAN_4_BIT_MASK                      0xfU
#define CAN_5_BIT_MASK                      0x1fU

/* ---- bit position ---- */
#define CAN_B0_BIT_POS                      0U
#define CAN_B1_BIT_POS                      1U
#define CAN_B2_BIT_POS                      2U
#define CAN_B3_BIT_POS                      3U
#define CAN_B4_BIT_POS                      4U
#define CAN_B5_BIT_POS                      5U
#define CAN_B6_BIT_POS                      6U
#define CAN_B7_BIT_POS                      7U
#define CAN_B8_BIT_POS                      8U
#define CAN_B9_BIT_POS                      9U
#define CAN_B10_BIT_POS                     10U
#define CAN_B11_BIT_POS                     11U
#define CAN_B12_BIT_POS                     12U
#define CAN_B13_BIT_POS                     13U
#define CAN_B14_BIT_POS                     14U
#define CAN_B15_BIT_POS                     15U

#define CAN_CRBRCF0_RX_BUF_NUM              32U
#define CAN_CRBRCF1_RX_BUF_NUM              32U
#define CAN_CRBRCF2_RX_BUF_NUM              32U


/* ---- TX buffer, TX status flag ---- */
#define CAN_TBTST_NOT_TRANSMITTING          0U
#define CAN_TBTST_TRANSMITTING              1U

/* ---- bit operations ---- */
#define GET_BIT(reg, pos)              (((reg) >> (pos)) & 1U)
#define SET_BIT(reg, pos)              ((reg) |= 1U << (pos))
#define CLR_BIT(reg, pos)              ((reg) &= ~(1UL << (pos)))



#define USE_RX_FIFO_BUFFER      (1U)
#define USE_TX_FIFO_BUFFER      (1U)

/*
*******************************************************************************
**  Function define
******************************************************************************/
void R_RSCAN0FD_Init(void);
// void Canfd_ReadRxBuffer(CANFD_FrameType* pFrame);
void R_RSCANFD_ReadRxFIFOBuffer(CANFD_FrameType* pFrame);
// CAN_STATUS_t R_RSCAN0FD_C1TrmByTxBuf(uint8_t TxBufIdx, const CANFD_FrameType* pFrame);
void CANFD_ReadHwRxFIFOBuffer(CANFD_FrameType* pFrame);
CAN_STATUS_t CANFD_TransmitHwTxFIFOBuffer(CANFD_FrameType* pFrame);



#endif /* R_RSCANFD_H */
