/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : Config_TAUB0_0_user.c
* Version      : 1.2.0
* Device(s)    : R7F701686
* Description  : This file implements device driver for Config_TAUB0_0.
* Creation Date: 2021-08-18
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_TAUB0_0.h"
/* Start user code for include. Do not edit comment generated here */
#include "r_rscanfd.h"
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
int TAUBcnt = 0;
extern CAN_STATUS_t temp;
extern volatile int a;
extern volatile long long *data, ans, id;
extern CANFD_FrameType pDemo;// = (CANFD_FrameType*)malloc(sizeof(CANFD_FrameType));

extern uint32_t gu32TxCnt;
extern int ERFL_0, ERFL_0_3;
extern CANFD_FrameType CANFD_TraStandData;

extern int gu32CANResumeFlag;
extern int gu32Can0ErrIntCntCpy, gu32Can0ErrIntCnt;
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_Config_TAUB0_0_Create_UserInit
* Description  : This function adds user code after initializing the TAUB00 channel
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_Config_TAUB0_0_Create_UserInit(void)
{
    /* Start user code for user init. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_Config_TAUB0_0_interrupt
* Description  : This function is TAUB00 interrupt service routine
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma interrupt r_Config_TAUB0_0_interrupt(enable=true, channel=142, fpu=true, callt=false)
void r_Config_TAUB0_0_interrupt(void)
{
    /* Start user code for r_Config_TAUB0_0_interrupt. Do not edit comment generated here */
    TAUBcnt++;
    if( TAUBcnt == 10){
        R_RSCAN0FD_Init();
    }
    else if(TAUBcnt > 10){
        if(gu32CANResumeFlag == 1){
                gu32CANResumeFlag = 0;
        }
        else{
        //if(gu32Can0ErrIntCntCpy == gu32Can0ErrIntCnt){
                while(!(RCFDC0.CFDCFSTS0.UINT32 & 0x1U));
                temp = CANFD_TransmitHwTxFIFOBuffer(&CANFD_TraStandData);
                ERFL_0 = RCFDC0.CFDC0ERFL.UINT32;
                if(temp == CAN_OK){
                    gu32TxCnt++;
                }
                
                CANFD_ReadHwRxFIFOBuffer(&pDemo);
                //ERFL_0 = RCFDC0.CFDC0ERFL.UINT32;
                id = pDemo.ID;
                data = &pDemo.DB;
                ans = *data; 
        }
    }
    //gu32Can0ErrIntCntCpy = gu32Can0ErrIntCnt;
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
