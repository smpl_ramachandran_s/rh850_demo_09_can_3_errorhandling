/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_main.c
* Version      : 1.0.100
* Device(s)    : R7F701686
* Description  : This function implements main function.
* Creation Date: 2021-08-18
***********************************************************************************************************************/
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_TAUB0_0.h"
#include "r_cg_cgc.h"
/* Start user code for include. Do not edit comment generated here */
#include "r_rscanfd.h"
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
volatile int gu32TxCnt, gu32RxIntCnt, gu32Can0ErrIntCnt, gu32Can0ErrIntCntCpy, gu32CanGlobalErrIntCnt, flag = 0;
volatile int ERFL_0, ERFL_0_2, ERFL_0_3;

CAN_STATUS_t temp;
volatile int a = 0;
volatile long long *data, ans = 0, id = 0;
CANFD_FrameType pDemo;// = (CANFD_FrameType*)malloc(sizeof(CANFD_FrameType));
int gu32CANResumeFlag = 0;

CANFD_FrameType CANFD_TraStandData={
  0x18,     /* ID */
  0x0,      /* THLEN */
  0x0,      /* RTR */
  0x0,      /* IDE */
  0x0000,   /* TS */                            
  0x00,     /* LBL */
  0x0,      /* res0 */
  0x8,      /* DLC */
  0x0,      /* ESI */
  0x0,      /* BRS */
  0x0,      /* FDF */
  0x0000,   /* res1 */
  {
    0x12,   /* DB0 */
    0x34,   /* DB1 */
    0x56,   /* DB2 */
    0x78,   /* DB3 */
    0x87,   /* DB4 */
    0x65,   /* DB5 */
    0x43,   /* DB6 */
    0x21,   /* DB7 */
  }
};

void init_ports(void);
void R_RSCAN0FD_ErrorClear(void);
/* End user code. Do not edit comment generated here */
void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void main(void)
{
    R_MAIN_UserInit();
    /* Start user code for main. Do not edit comment generated here */
    R_Config_TAUB0_0_Start();
    while(1){
//        while(!(RCFDC0.CFDCFSTS0.UINT32 & 0x1U));
//        temp = CANFD_TransmitHwTxFIFOBuffer(&CANFD_TraStandData);
//        ERFL_0 = RCFDC0.CFDC0ERFL.UINT32;
//        if(u32ErrResFlag < 25){
//            R_RSCAN0FD_ErrorClear();
//            u32ErrResFlag++;
//        }
//        if(temp == CAN_OK){
//            gu32TxCnt++;
//        }
        
//        CANFD_ReadHwRxFIFOBuffer(&pDemo);
//        //ERFL_0 = RCFDC0.CFDC0ERFL.UINT32;
//        id = pDemo.ID;
//        data = &pDemo.DB;
//        ans = *data;  
        
        //EI();
    }
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    DI();
    /* Start user code for R_MAIN_UserInit. Do not edit comment generated here */
    R_Systeminit();
    init_ports();
    //R_Config_TAUB0_0_Create();
    R_RSCAN0FD_Init();
    /* End user code. Do not edit comment generated here */
    R_Systeminit();
    EI();
}

/* Start user code for adding. Do not edit comment generated here */
void init_ports(void){
    PORT.PIBC0 &= ( _PORT_CLEAR_BIT0 & _PORT_CLEAR_BIT1 & _PORT_CLEAR_BIT2);    // 0xFF7FU
    PORT.PBDC0 &= ( _PORT_CLEAR_BIT0 & _PORT_CLEAR_BIT1 & _PORT_CLEAR_BIT2);
    PORT.PM0 &= ( _PORT_CLEAR_BIT0 & _PORT_CLEAR_BIT1 & _PORT_CLEAR_BIT2);
    PORT.PMC0 &= ( _PORT_CLEAR_BIT0 & _PORT_CLEAR_BIT1 & _PORT_CLEAR_BIT2);
    // P0_0: CAN0Tx  ->  001 0
    PORT.PFC0 |= _PORT_SET_BIT0;
    PORT.PFCE0 &= _PORT_CLEAR_BIT0;
    PORT.PFCAE0 &= _PORT_CLEAR_BIT0;
    // P0_1: CAN0Rx  ->  110 1

    PORT.PFC0 |= _PORT_SET_BIT1;
    PORT.PFCE0 &= _PORT_CLEAR_BIT1;
    PORT.PFCAE0 &= _PORT_CLEAR_BIT1;
    // P0_2: CAN0 Enable  -> Active low

    PORT.PMC0 |= ( _PORT_SET_BIT0 | _PORT_SET_BIT1);
    PORT.PMC0 &= _PORT_CLEAR_BIT2;
    PORT.PM0 &= ( _PORT_CLEAR_BIT0 & _PORT_CLEAR_BIT2);
    PORT.PM0 |= _PORT_SET_BIT1;
    PORT.P0 &= _PORT_CLEAR_BIT2;

    // Enabling Power Supply to CAN chip
    PORT.PIBC9 &= _PORT_CLEAR_BIT3;
    PORT.PBDC9 &= _PORT_CLEAR_BIT3;
    PORT.PMC9 &= _PORT_CLEAR_BIT3;
    PORT.PM9 &= _PORT_CLEAR_BIT3;
    PORT.P9 |= _PORT_SET_BIT3;
}

void R_RSCAN0FD_ErrorClear(void){
    RCFDC0.CFDGCTR.UINT32 |= 0x00000001u;  //GMDC[1:0]BIT = 01B  ---ensure from global operation mode to reset mode
    while(RCFDC0.CFDGSTS.UINT32 !=0x01);
    
    RCFDC0.CFDC0CTR.UINT32 &= 0xfffffffb;
    
    /* If GlobalChannel in halt or reset mode */
    if (RCFDC0.CFDGSTS.UINT32 & 0x03) 
    {
        RCFDC0.CFDGCTR.UINT32 &= 0xfffffffc; //Switch to communication mode
        while ((RCFDC0.CFDGSTS.UINT32 & 0x01) == 1) {
            /* While reset mode */
        }
    }
    
    /* If Channel 0 in halt or reset mode */
    if (RCFDC0.CFDC0STS.UINT32 & 0x03) 
    {
        RCFDC0.CFDC0CTR.UINT32 &= 0xfffffffc;    //Switch to communication mode
        while ((RCFDC0.CFDC0STS.UINT32 & 0x01) == 1) {
            /* While reset mode */
        }
    }
   
   
#if (USE_RX_FIFO_BUFFER == 1)
    /* Following configuration is done in channel communication mode (operation mode). */
    RCFDC0.CFDRFCC0.UINT32 |= 0x01; /* Receive FIFO buffers are used */
#endif  /* (USE_RX_FIFO_BUFFER == 1) */
#if (USE_TX_FIFO_BUFFER == 1)
    RCFDC0.CFDCFCC0.UINT32 |= 0x01; /* Transmit/receive FIFO buffers are used */
#endif  /* (USE_TX_FIFO_BUFFER == 1) */

}

#pragma interrupt r_Config_RSCAN0_Receive_FIFO_interrupt(enable=true, channel=23, fpu=true, callt=false)
void r_Config_RSCAN0_Receive_FIFO_interrupt(void)
{
    // /* Start user code for r_Config_ADCA0_error_interrupt. Do not edit comment generated here */
    gu32RxIntCnt++;
    // /* End user code. Do not edit comment generated here */
}

#pragma interrupt r_Config_RSCAN0_Error_interrupt(enable=true, channel=24, fpu=true, callt=false)
void r_Config_RSCAN0_Error_interrupt(void)
{
    // /* Start user code for r_Config_ADCA0_error_interrupt. Do not edit comment generated here */
    gu32Can0ErrIntCnt++;
    ERFL_0_3 = RCFDC0.CFDC0ERFL.UINT32;
    R_RSCAN0FD_ErrorClear();
    //NOP();
    //while(1);
    //R_RSCAN0FD_Init();
    ERFL_0_2 = RCFDC0.CFDC0ERFL.UINT32;
    gu32CANResumeFlag = 1;
    //while(1);
    // /* End user code. Do not edit comment generated here */
}

#pragma interrupt r_Config_RSCAN0_Global_Error_interrupt(enable=true, channel=22, fpu=true, callt=false)
void r_Config_RSCAN0_Global_Error_interrupt(void)
{
    // /* Start user code for r_Config_ADCA0_error_interrupt. Do not edit comment generated here */
    gu32CanGlobalErrIntCnt++;
    // /* End user code. Do not edit comment generated here */
}
/* End user code. Do not edit comment generated here */
